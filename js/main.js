const img_s = 64;
const img_p = 'img/';
const score_o = 0;
const main_o = 1;
const quiz_o = 2;
const end_o = 3;
const ht_k = 'scores';
const th_t = '<tr><th>Username</th><th>Score</th></tr>';
const qu_req_p = 'https://opentdb.com/api.php?amount=5&token=';
const tk_req_p = 'https://opentdb.com/api_token.php?command=request';


function render(timestamp) {
  imgpos.f.x += (mospos.x - imgpos.f.x) * 0.1;
  imgpos.f.y += (mospos.y - imgpos.f.y) * 0.1;
  imgpos.m.x += (imgpos.f.x - imgpos.m.x) * 0.1;
  imgpos.m.y += (imgpos.f.y - imgpos.m.y) * 0.1;
  imgpos.b.x += (imgpos.m.x - imgpos.b.x) * 0.1;
  imgpos.b.y += (imgpos.m.y - imgpos.b.y) * 0.1;
  //console.log(imgpos);
  ctx.drawImage(bimg, -imgpos.b.x, -imgpos.b.y);
  ctx.drawImage(mimg, -imgpos.m.x, -imgpos.m.y);
  ctx.drawImage(fimg, -imgpos.f.x, -imgpos.f.y);
  requestAnimationFrame(render);
}


function updatePos(event) {
  mospos.x = event.clientX * .25;
  mospos.y = event.clientY * .25;
}


function toLoc(loc) {
  let d = loc * -25;
  container.style.transform = `translateX(${d}%)`;
}


async function initQuiz() {
  showWaiter();
  scoval = 0, maxscoval = 0, que_m.value = 0;
  quests = await getQuestions(token);
  if (!quests.response_code) {
    token = await getToken();
    quests = await getQuestions(token);
  }
  setTimeout(() => {
    toLoc(quiz_o);
    queid = -1;
    nextQuiz();
  }, 250);
  setTimeout(hideWaiter, 500);
}


function randomizeQuiz() {
  anstmp = {a: quests.results[queid].incorrect_answers.slice(), c: null};
  anstmp.a.push(quests.results[queid].correct_answer);
  let swap = (i, a) => {let t = a[i]; a[i] = a[i+1]; a[i+1] = t;}
  for (let i = 0; i < 64; i++) {
    swap(anstmp.a.length === 2 ? 0 : Math.floor(Math.random() * 3), anstmp.a);
  }
  anstmp.c = anstmp.a.indexOf(quests.results[queid].correct_answer);
}


function clearQuiz() {
  for (let e of queseldom) {
    e.className = '';
  }
}


function displayQuiz() {
  textWash(quests.results[queid].question, quequedom);
  for (let i = 0; i < queseldom.length; i++)
    anstmp.a[i] === undefined ? queseldom[i].className = 'hide' : textWash(anstmp.a[i], queseldom[i]);
}


function checkQuiz(ansid) {
  let m;
  switch(quests.results[queid].difficulty) {
    case 'easy':
      m = 1;
      break;
    case 'medium':
      m = 2;
      break;
    case 'hard':
      m = 3;
      break;
  };
  if (ansid === anstmp.c) scoval += 5 * m;
  maxscoval += 5 * m;
  lightQuiz(ansid);
}


function lightQuiz(ansid) {
  try {queseldom[ansid].className = 'wrng';} catch (e) {}
  queseldom[anstmp.c].className = 'corr';
}


function nextQuiz() {
  try {
    que_m.value = ++queid + 1;
    randomizeQuiz();
    clearQuiz();
    displayQuiz();
  } catch (e) {
    endnamebtndom.disabled = false;
    toLoc(end_o);
    scodisdom.innerText = `Your score: ${scoval} / ${maxscoval}`;
  }
}


async function getToken() {
  const resp = await fetch(tk_req_p);
  const json = await resp.json();
  return await json['token'];
}


async function getQuestions(token) {
  const resp = await fetch(qu_req_p+token);
  const json = await resp.json();
  return await json;
}


function addScore(name, val, maxval) {
  scotabl = JSON.parse(localStorage.getItem(ht_k));
  if (scotabl === undefined) {
    scotabl = {};
  }
  scotabl[name] = {s: val, m: maxval};
  localStorage.setItem(ht_k, JSON.stringify(scotabl));
  createTabl(tabdom, scotabl);
}


function submit() {
  addScore(endnameinpdom.value, scoval, maxscoval);
  endnamebtndom.disabled = true;
}


function loadScores() {
  scotabl = JSON.parse(localStorage.getItem(ht_k));
  createTabl(tabdom, scotabl);
}


function createTabl(dom, obj) {
  dom.innerHTML = th_t;
  for (let i in obj) {
    dom.innerHTML += `<tr><td>${i}</td><td>${obj[i].s} / ${obj[i].m}</td></tr>`;
  }
}


function textWash(strTo, elem, t=500) {
  elem.onanimationiteration = () => {elem.style.animationPlayState = 'paused';};
  elem.style.animationPlayState = 'running';
  setTimeout(() => {elem.innerHTML = strTo}, t / 2);
}


function showWaiter() {
  waitdom.style.zIndex = 2;
  waitdom.style.opacity = 1;
}


function hideWaiter() {
  waitdom.style.opacity = 0;
  setTimeout(() => {waitdom.style.zIndex = -1;}, 500);
}


let mospos, imgpos, can, ctx, fimg, mimg, bimg;
let maindom, quizdom, scoredom, enddom;
let tabdom;
let container;
let scotabl = {};
let scoval = 0, maxscoval = 0;
let token, quests, anstmp, queid, queseldom, quequedom, quemetdom;
let endnameinpdom, endnamebtndom, scodisdom;
let waitdom;


window.addEventListener('load', async () => {
    token = await getToken();
    waitdom = document.getElementsByClassName('waiter')[0];
    fimg = new Image();
    mimg = new Image();
    bimg = new Image();
    fimg.src = img_p+'foreground.png';
    mimg.src = img_p+'midground.png';
    bimg.src = img_p+'background.png';

    mospos = {x: 0, y: 0};
    imgpos = {f: {x: 0, y: 0}, m: {x: 0, y: 0}, b: {x: 0, y: 0}};

    container = document.getElementsByClassName('container')[0];
    maindom = document.getElementsByClassName('main')[0];
    quizdom = document.getElementsByClassName('quiz')[0];
    scoredom = document.getElementsByClassName('scores')[0];
    enddom = document.getElementsByClassName('end')[0];
    tabdom = document.getElementsByTagName('table')[0];
    queseldom = document.getElementsByClassName('quiz_sel')[0].children;
    quequedom = document.getElementById('que_p');
    quemetdom = document.getElementById('que_m');
    scotabl = loadScores();
    let enddoms = document.getElementsByClassName('name_inp');
    endnameinpdom = enddoms[0];
    endnamebtndom = enddoms[1];
    scodisdom = document.getElementById('enddis');
    delete enddoms;

    can = document.getElementById('canvas');
    can.width = window.innerWidth; can.height = window.innerHeight;
    ctx = can.getContext('2d');

    window.addEventListener('resize', () => {
      can.width = window.innerWidth;
      can.height = window.innerHeight;
    });

    window.addEventListener('mousemove', updatePos);
    requestAnimationFrame(render);

    hideWaiter();
});
